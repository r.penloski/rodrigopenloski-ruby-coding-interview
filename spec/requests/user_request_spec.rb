require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1), params: { company_identifier: company_1.to_param }
        expect(result.size).to eq(company_1.users.size)
        expect(result.map { |element| element['id'] } ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching all users' do
      include_context 'with multiple companies'

      it 'returns all the users' do

      end
    end

    context 'when filtering by name' do
      include_context 'with multiple companies'

      let!(:john) { create(:user, username: 'John', company: company_1) }
      let!(:joan) { create(:user, username: 'Joan', company: company_1) }

      it 'returns partial matches' do
        get company_users_path(company_1), params: { username: 'Jo' }
        expect(result.size).to eq(2)
      end

      it 'returns whole matches' do
        get company_users_path(company_1), params: { username: 'John' }
        expect(result.size).to eq(1)
      end

      it 'returns all user' do
        get company_users_path(company_1), params: { username: '' }
        expect(result.size).to eq(User.count)
      end
    end
  end
end
